from django.conf.urls import include, url
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from library.forms import FormCreateBook, FormCreateAuthor
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from . import views

urlpatterns = [
    url(r'^$' ,views.BookListView.as_view(),name='list-book'),
    url(r'^(?P<au_slug>(\w+))/(?P<year_slug>(\w+))/$' ,views.BookListView.as_view(),name='filter-book'),
    url(r'^login/', views.login, name='login'),
    url(r'^logout/', views.logout, name='logout'),
    url(r'^create-book$', login_required(views.BookCreateView.as_view()), name='create'),
    url(r'^(?P<pk>\d+)/update-book$', login_required(views.BookUpdateView.as_view()), name='update'),
    url(r'^(?P<pk>\d+)/delete-book$', login_required(views.BookDeleteView.as_view()), name='delete'),
    url(r'^(?P<pk>\d+)/detail-book$', login_required(views.BookDetailView.as_view()), name='detail-book'),
    url(r'^create-author$', login_required(views.AuthorCreateView.as_view()), name='create-author'),
]
