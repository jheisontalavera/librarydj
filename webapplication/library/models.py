from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.

class Author(models.Model):
	'''An author has first, last name and nationality'''
	first_name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=40)
	nationality = models.CharField(max_length=30, blank=True)

	def __unicode__(self):
		full_name = "%s %s" %(self.first_name, self.last_name)
		return full_name
	class Meta:
		verbose_name_plural = 'Authors'

class Book(models.Model):
	title = models.CharField(max_length=100)
	authors = models.ManyToManyField(Author)
	description = models.TextField(blank=True)	
	edition = models.IntegerField(default=1,
        validators=[MinValueValidator(1)])
	year_publication = models.IntegerField(default=1,
        validators=[MinValueValidator(1)])
	num_copies = models.IntegerField(default=1,
        validators=[MinValueValidator(1)])
	image = models.ImageField(upload_to = 'images/')
	
	
	def __unicode__(self):
		return self.title
	
	class Meta:
		verbose_name_plural = 'Books'
