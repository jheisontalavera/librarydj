from django import forms 
from django.forms import ModelForm

from library.models import Book, Author

class FormCreateBook(forms.ModelForm):
    class Meta:
        model = Book
    	fields = ['title', 'authors','edition','num_copies', 'year_publication', 'image', 'description']

class FormCreateAuthor(forms.ModelForm):
    class Meta:
        model = Author
    	fields = ['first_name', 'last_name','nationality']
