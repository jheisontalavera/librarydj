from django.shortcuts import render
from library.models import Author, Book
from library.forms import FormCreateBook, FormCreateAuthor
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse_lazy
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.views.decorators.cache import never_cache

from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.views import login as auth_login, logout as auth_logout
# Create your views here.

@never_cache
def login(request):
    return auth_login(request, 'library/login.html')


@never_cache
def logout(request):
    return auth_logout(request, template_name='library/login.html')



class BookListView(ListView):
    model = Book
    paginate_by = 3
    template_name = 'library/home.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(BookListView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        try:
            author = self.kwargs['au_slug']
            if author != '0':
                object_list = self.model.objects.filter(authors__id = author)
        except:
            author = ''

        try:
            year_slug = self.kwargs['year_slug']
            if year_slug != '0':
                object_list = self.model.objects.filter(year_publication = year_slug)
        except:
            year_slug = ''

        if self.request.GET.get("search"):
            search = self.request.GET.get("search")
            object_list = self.model.objects.filter(Q(title__icontains = search) | Q(authors__first_name__icontains=search) | Q(authors__last_name__icontains=search) )
        else:
            search = ''

        if (not(year_slug) or  not(author)) and not(search):
            object_list = self.model.objects.all()

        return object_list

    def get_context_data(self,**kwargs):
        context = super(BookListView, self).get_context_data(**kwargs)
        context['authors'] = Author.objects.all()
        context['year_publication'] = Book.objects.order_by('-year_publication').values('year_publication').distinct()
        return context

class BookCreateView(CreateView):
    model = Book
    form_class = FormCreateBook
    success_url="/library"
    template_name = 'library/fm_book.html'

class BookUpdateView(UpdateView):
    model = Book
    form_class = FormCreateBook
    success_url="/library"
    template_name = 'library/fm_book.html'

class BookDeleteView(DeleteView):
    model = Book
    success_url = "/library"

class BookDetailView(DetailView):
    model = Book
    template_name = 'library/detailbook.html'

class AuthorCreateView(CreateView):
    model = Author
    form_class = FormCreateAuthor
    template_name = 'library/fm_author.html'
    success_url = "/library"
